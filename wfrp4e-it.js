
Hooks.on("setup", () => {
    game.settings.register("wfrp4e-core", "initialized", {
        name: "Initialization",
        scope: "world",
        config: false,
        default: false,
        type: Boolean
    });

    const WFRP4E = {}

    // Species
    WFRP4E.species = {
        "human": "Umano",
        "dwarf": "Nano",
        "halfling": "Halfling",
        "helf": "Elfo Alto",
        "welf": "Elfo Silvano",
    };

    WFRP4E.speciesSkills = {
        "human": [
            "Affascinare",
            "Allevamento",
            "Comandare",
            "Conoscenza(Reikland)",
            "Freddezza",
            "Lingua(Bretoniano)",
            "Lingua(Marienburghese)",
            "Mercanteggiare",
            "Mira(Archi)",
            "Miaschia(Base)",
            "Pettegolezzi",
            "Valutare"
        ],
        "dwarf": [
            "Conoscenza(Geologia)",
            "Conoscenza(Metallurgia)",
            "Conoscenza(Nani)",
            "Freddezza",
            "Intimidire",
            "Intrattenere(Cantastorie)",
            "Lingua(Khazalid)",
            "Mestiere(Qualsiasi)",
            "Mischia(Base)",
            "Reggere Alcolici",
            "Tempra",
            "Valutare"
        ],
        "halfling": [
            "Affascinare",
            "Conoscenza(Raikland)",
            "Giocare d'Azzardo",
            "Intuire",
            "Lingua(Contradano)",
            "Mercanteggiare",
            "Mestiere(Cuoco)",
            "Nascondersi(Qualsiasi)",
            "Percepire",
            "Rapidità di Mano",
            "Reggere Alcolici",
            "Schivare"
        ],
        "helf": [
            "Comandare",
            "Freddezza",
            "Intrattenere(Canto)",
            "Lingua(Eltharin)",
            "Mira(Archi)",
            "Mischia(Base)",
            "Navigare",
            "Nuotare",
            "Orientarsi",
            "Percepire",
            "Suonare(Qualsiasi)",
            "Vlutare"
        ],
        "welf": [
            "Atletica",
            "Arrampicarsi",
            "Intrattenere(Canto)",
            "Intimidire",
            "Lingua(Eltharin)",
            "Mira(Archi)",
            "Mischia(Base)",
            "Nascondersi(Campagne)",
            "Seguire Tracce",
            "Sopravvivenza",
            "Percepire",
            "Tempra"
        ],
    }

    WFRP4E.speciesTalents = {
        "human": [
            "Predestinato",
            "Buon Senso, Cortese",
            3
        ],
        "dwarf": [
            "Leggere/Scrivere, Inesorabile",
            "Resistente alla Magia",
            "Risoluto, Determinato",
            "Vigoroso",
            "Vista Notturna",
            0
        ],
        "halfling": [
            "Piccolo",
            "Resistente(Chaos)",
            "Senso Acuto(Gusto)",
            "Vista Notturna",
            2
        ],
        "helf": [
            "Buon Senso, Razionale",
            "Leggere/Scrivere",
            "Seconda Vista, Sesto Senso",
            "Senso Acuto(Vista)",
            "Vista Notturna",
            0
        ],
        "welf": [
            "Senso Acuto(Vista)",
            "Gagliardo, Seconda Vista",
            "Vista Notturna",
            "Leggere/Scrivere, Saldo",
            "Girovago",
            0
        ]
    }

    WFRP4E.speciesHeight = {
        "human": {
            feet: 4,
            inches: 9,
            die: "2d10"
        },
        "dwarf": {
            feet: 4,
            inches: 3,
            die: "1d10"
        },
        "halfling": {
            feet: 3,
            inches: 1,
            die: "1d10"
        },
        "helf": {
            feet: 5,
            inches: 11,
            die: "1d10"
        },
        "welf": {
            feet: 5,
            inches: 11,
            die: "1d10"
        }
    }


    WFRP4E.loreEffectDescriptions = {
        "petty": "None",
        "beasts": "WFRP4E.LoreDescription.Beasts",
        "death": "WFRP4E.LoreDescription.Death",
        "fire": "WFRP4E.LoreDescription.Fire",
        "heavens": "WFRP4E.LoreDescription.Heavens",
        "metal": "WFRP4E.LoreDescription.Metal",
        "life": "WFRP4E.LoreDescription.Life",
        "light": "WFRP4E.LoreDescription.Light",
        "shadow": "WFRP4E.LoreDescription.Shadow",
        "hedgecraft": "WFRP4E.LoreDescription.Hedgecraft",
        "witchcraft": "WFRP4E.LoreDescription.Witchcraft",
        "daemonology": "",
        "necromancy": "",
        "nurgle": "",
        "slaanesh": "",
        "tzeentch": "WFRP4E.LoreDescription.Tzeentch",
    };


    WFRP4E.symptoms = {
        "blight": "Consunzione",
        "buboes": "Bubboni",
        "convulsions": "Convulsioni",
        "coughsAndSneezes": "Tosse e Starnuti",
        "fever": "Febbre",
        "flux": "Diarrea",
        "gangrene": "Cancrena",
        "lingering": "Persistenza",
        "malaise": "Malessere",
        "nausea": "Nausea",
        "pox": "Vescicole",
        "wounded": "Lesioni",
        "delirium": "Delirio",
        "swelling": "Rigonfiamento"
    }

    WFRP4E.locations = {
        "head": "Testa",
        "body": "Corpo",
        "rArm": "Braccio Destro",
        "lArm": "Braccio Sinistro",
        "rLeg": "Gamba Destra",
        "lLeg": "Gamba Sinistra",
    }
    
    WFRP4E.creditOptions = {
        SPLIT: "dividi",
        EACH: "ognuno",
    }


    // Armor Qualities
    WFRP4E.armorQualities = {
        "flexible": "Flessibile",
        "impenetrable": "Impenetrabile",
    };

    // Armor Flaws
    WFRP4E.armorFlaws = {
        "partial": "Parziale",
        "weakpoints": "Punto Debole",
    };

    
    // Range Test Modifiers
    WFRP4E.rangeModifiers = {
        "Point Blank": "Facile (+40)",
        "Short Range": "Media (+20)",
        "Normal": "Impegnativa (+0)",
        "Long Range": "Difficile (-10)",
        "Extreme": "Molto Ardua (-30)",
    }

    // Difficulty Labels
    WFRP4E.difficultyLabels = {

        "veasy": "Molto Facile (+60)",
        "easy": "Facile (+40)",
        "average": "Media (+20)",
        "challenging": "Impegnativa (+0)",
        "difficult": "Difficile (-10)",
        "hard": "Ardua (-20)",
        "vhard": "Molto Ardua(-30)",
        "futil": "Futile (-40)",
        "imposs": "Impossibile (-50)"
    }

    WFRP4E.locations = {
        "head": "Testa",
        "body": "Corpo",
        "rArm": "Braccio Destro",
        "lArm": "Braccio Sinistro",
        "rLeg": "Gamba Sinistra",
        "lLeg": "Gamba Destra",
    }


    // Condition Types
    WFRP4E.magicLores = {
        "petty": "Minori",
        "beasts": "Bestie",
        "death": "Morte",
        "fire": "Fuoco",
        "heavens": "Empireo",
        "metal": "Metallo",
        "life": "Vita",
        "light": "Luce",
        "shadow": "Ombra",
        "hedgecraft": "Soglia",
        "witchcraft": "Stregoneria",
        "daemonology": "Demonologia",
        "necromancy": "Necromanzia",
        "nurgle": "Nurgle",
        "slaanesh": "Slaanesh",
        "tzeentch": "Tzeentch",
    };

    // Given a Lore, what is the Wind
    WFRP4E.magicWind = {
        "petty": "Nessuno",
        "beasts": "Ghur",
        "death": "Shyish",
        "fire": "Aqshy",
        "heavens": "Azyr",
        "metal": "Chamon",
        "life": "Ghyran",
        "light": "Hysh",
        "shadow": "Ulgu",
        "hedgecraft": "Nessuno",
        "witchcraft": "Nessuno",
        "daemonology": "Dhar",
        "necromancy": "Dhar",
        "nurgle": "Dhar",
        "slaanesh": "Dhar",
        "tzeentch": "Dhar",
    };



    // Types of prayers
    WFRP4E.prayerTypes = {
        "blessing": "Preghiera",
        "miracle": "Miracolo"
    }

    WFRP4E.mutationTypes = {
        "physical": "Fisica",
        "mental": "Mentale"
    }

    
    WFRP4E.loreEffects = {
        "beasts": {
            label: "Sapere delle Bestie",
            icon: "modules/wfrp4e-core/icons/spells/beasts.png",
            transfer: true,
            flags: {
                wfrp4e: {
                    "effectApplication": "actor",
                    "effectTrigger": "invoke",
                    "lore": true,
                    "script": `
                    let value = 1
                    let name = this.actor.data.token.name
                    
                    if (game.user.isGM) {
                        game.user.targets.forEach(t => {
                            t.actor.applyFear(value, name)
                        })
                        game.user.updateTokenTargets([]);
                    }
                    else {
                        game.wfrp4e.utility.postFear(value, name)
                    }
                `
                }
            }
        },
        "death": {
            label: "Sapere della Morte",
            icon: "modules/wfrp4e-core/icons/spells/death.png",
            transfer: true,
            flags: {
                wfrp4e: {
                    "effectApplication": "apply",
                    "effectTrigger": "oneTime",
                    "lore": true,
                    "script": `
                    if (args.actor.owner)
                    {
                        args.actor.addCondition("fatigued")
                    }`
                }
            }
        },
        "fire": {
            label: "Sapere del Fuoco",
            icon: "modules/wfrp4e-core/icons/spells/fire.png",
            transfer: true,
            flags: {
                wfrp4e: {
                    "effectApplication": "apply",
                    "effectTrigger": "oneTime",
                    "lore": true,
                    "script": `
                    if (args.actor.owner)
                    {
                        args.actor.addCondition("ablaze")
                    }`
                }
            }
        },
        "heavens": {
            label: "Sapere dell'Empireo",
            icon: "modules/wfrp4e-core/icons/spells/heavens.png",
            transfer: true,
            flags: {
                wfrp4e: {
                    "effectApplication": "damage",
                    "effectTrigger": "applyDamage",
                    "lore": true,
                    "script": `
                let applyAP = (args.damageType == game.wfrp4e.config.DAMAGE_TYPE.IGNORE_TB || args.damageType == game.wfrp4e.config.DAMAGE_TYPE.NORMAL)
                
                let AP = args.AP
                let metalValue = 0;
                for (let layer of AP.layers) {
                   if (layer.metal) {
                      metalValue += layer.value
                   }
                }
                
                if (applyAP) {
                
                   args.totalWoundLoss +=  metalValue
                   let newUsed = AP.used - metalValue;
                
                   let apIndex = args.messageElements.findIndex(i => i.includes(game.i18n.localize("AP")))
                   args.messageElements[apIndex] = newUsed + "/" + AP.value + " " + game.i18n.localize("AP")
                }
                    `
                }
            }
        },
        "metal": {
            label: "Sapere del Metallo",
            icon: "modules/wfrp4e-core/icons/spells/metal.png",
            transfer: true,
            flags: {
                wfrp4e: {
                    "effectApplication": "damage",
                    "effectTrigger": "applyDamage",
                    "lore": true,
                    "script": `
                let applyAP = (args.damageType == game.wfrp4e.config.DAMAGE_TYPE.IGNORE_TB || args.damageType == game.wfrp4e.config.DAMAGE_TYPE.NORMAL)

                let AP = args.AP
                let metalValue = 0;
                for (let layer of AP.layers) {
                   if (layer.metal) {
                      metalValue += layer.value
                   }
                }
                
                
                if (metalValue)
                    args.messageElements.push("-2 Metal Armour")
                args.totalWoundLoss += metalValue
                
                if (applyAP) {
                
                   args.totalWoundLoss +=  metalValue
                   let newUsed = AP.used - metalValue;
                
                   let apIndex = args.messageElements.findIndex(i => i.includes(game.i18n.localize("AP")))
                   args.messageElements[apIndex] = newUsed + "/" + AP.value + " " + game.i18n.localize("AP")
                }
                `
                }
            }
        },
        "life": {
            label: "Sapere della Vita",
            icon: "modules/wfrp4e-core/icons/spells/life.png",
            transfer: true,
            flags: {
                wfrp4e: {
                    "effectApplication": "apply",
                    "effectTrigger": "oneTime",
                    "lore": true,
                    "script": `
                    fromUuid(this.effect.origin).then(caster => {
                        if (this.actor.owner)
                        {
                            if (!this.actor.has(game.i18n.localize("NAME.Daemonic")) && !this.actor.has(game.i18n.localize("NAME.Undead")))
                            {
                                let bleeding = this.actor.hasCondition("bleeding")
                                let fatigued = this.actor.hasCondition("fatigued")
                                if (bleeding) this.actor.removeCondition("bleeding", bleeding.flags.wfrp4e.value)
                                if (fatigued) this.actor.removeCondition("fatigued", fatigued.flags.wfrp4e.value)
                            }
                            else if (this.actor.has(game.i18n.localize("NAME.Undead")))
                                this.actor.applyBasicDamage(caster.data.data.characteristics.wp.bonus, {damageType : game.wfrp4e.config.DAMAGE_TYPE.IGNORE_ALL});
                        }
                    })`
                }
            }
        },
        "light": {
            label: "Sapere della Luce",
            icon: "modules/wfrp4e-core/icons/spells/light.png",
            transfer: true,
            flags: {
                wfrp4e: {
                    "effectApplication": "apply",
                    "effectTrigger": "oneTime",
                    "lore": true,
                    "script": `
                fromUuid(this.effect.origin).then(caster => {
                    if (this.actor.owner)
                    {
                        let bleeding = this.actor.addCondition("blinded")
                        if (this.actor.has(game.i18n.localize("NAME.Undead")) || this.actor.has(game.i18n.localize("NAME.Daemonic")))
                            this.actor.applyBasicDamage(caster.data.data.characteristics.int.bonus, {damageType : game.wfrp4e.config.DAMAGE_TYPE.IGNORE_ALL});
                    }
                })`
                }
            }
        },
        "shadow": {
            label: "Sapere delle Ombre",
            icon: "modules/wfrp4e-core/icons/spells/shadow.png",
            transfer: true,
            flags: {
                wfrp4e: {
                    "effectApplication": "damage",
                    "effectTrigger": "applyDamage",
                    "lore": true,
                    "script": `
                let applyAP = (args.damageType == game.wfrp4e.config.DAMAGE_TYPE.IGNORE_TB || args.damageType == game.wfrp4e.config.DAMAGE_TYPE.NORMAL)
                
                if (applyAP) {
                   let AP = args.AP
                
                   args.totalWoundLoss += AP.used
                   let apIndex = args.messageElements.findIndex(i => i.includes(game.i18n.localize("AP")))
                   args.messageElements[apIndex] = "0/" + AP.value + " " + game.i18n.localize("AP")
                }`
                }
            }
        },
        "hedgecraft": {
            label: "Sapere della Soglia",
            icon: "modules/wfrp4e-core/icons/spells/hedgecraft.png",
            transfer: true,
            flags: {
                wfrp4e: {
                    "effectApplication": "actor",
                    "effectTrigger": "invoke",
                    "lore": true,
                    "script": ``
                }
            }
        },
        "witchcraft": {
            label: "Sapere della Stregoneria",
            icon: "modules/wfrp4e-core/icons/spells/witchcraft.png",
            transfer: true,
            flags: {
                wfrp4e: {
                    "effectApplication": "apply",
                    "effectTrigger": "oneTime",
                    "lore": true,
                    "script": `
                    if (args.actor.owner)
                    {
                        args.actor.addCondition("bleeding")
                    }`
                }
            }
        },
        "tzeentch": {
            label: "Sapere di Tzeentch",
            icon: "modules/wfrp4e-core/icons/spells/tzeentch.png",
            transfer: true,
            flags: {
                wfrp4e: {
                    "effectApplication": "apply",
                    "effectTrigger": "oneTime",
                    "lore": true,
                    "script": `
                    if (args.actor.owner){
                        args.actor.setupSkill("Endurance", {context : {failure: "1 Corruption Point Gained", success : "1 Fortune Point Gained"}}).then(setupData => {
                            args.actor.basicTest(setupData).then(test => 
                            {
                                if (test.result.result == "success" && args.actor.data.type == "character")
                                {
                                    args.actor.update({"data.status.fortune.value" : args.actor.data.data.status.fortune.value + 1})
                                }
                                else if (test.result.result == "failure" && args.actor.data.type == "character")
                                {
                                args.actor.update({"data.status.corruption.value" : args.actor.data.data.status.corruption.value + 1})
                                }
                            })
                        }
                    }`
                }
            }
        }
    }

    for (let obj in WFRP4E) {
        for (let el in WFRP4E[obj]) {
            if (typeof WFRP4E[obj][el] === "string") {
                WFRP4E[obj][el] = game.i18n.localize(WFRP4E[obj][el])
            }
        }
    }

    mergeObject(game.wfrp4e.config, WFRP4E)

})
